package INF101.lab2;

import java.util.ArrayList;
//import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    

    List<FridgeItem> items = new ArrayList<FridgeItem>();
    List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
    List<FridgeItem> removeItems = new ArrayList<FridgeItem>();
    int max_size = 20;
    
    @Override
    public int nItemsInFridge() {
        int n = items.size();
        return n;
    }
    
    @Override
    public int totalSize() {
    
        return max_size;
    }

    
    public boolean isEmpty() {
        if (nItemsInFridge()==0){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge()<20) {
            items.add(item);
            return true;
        }
        else {
            return false;
        }
        
        
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!items.isEmpty()){
            for (FridgeItem fridgeItem : items) {
                if (fridgeItem.equals(item)) {
                    removeItems.add(item);
                }
            }
        }
        else {
            throw new NoSuchElementException();
        }
        items.removeAll(removeItems);

    }

    @Override
    public void emptyFridge() {
        items.clear();
        
    }
    
    @Override
    public List<FridgeItem> removeExpiredFood() { //Sjekke om gått ut. Fjerne fra items, og legge til i Expired
        
        for (FridgeItem item : items) {

            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        items.removeAll(expiredItems);
        return expiredItems;
    }

}